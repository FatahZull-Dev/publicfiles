-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 18, 2019 at 06:08 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `acis_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `work_items`
--

CREATE TABLE `work_items` (
  `id` int(11) NOT NULL,
  `p_id` varchar(255) DEFAULT NULL,
  `w_id` varchar(255) NOT NULL,
  `work_name` varchar(255) DEFAULT NULL,
  `w_img` varchar(255) NOT NULL,
  `date_submited` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `work_items`
--

INSERT INTO `work_items` (`id`, `p_id`, `w_id`, `work_name`, `w_img`, `date_submited`) VALUES
(1, 'P001', 's01', 'SUBSTATION', 'publicfiles/s01.png', '2019-11-19'),
(2, 'P001', 's02', 'EXISTING GEAR', 'publicfiles/s02.png', '2019-11-19'),
(3, 'P001', 's03', 'SWITCHGEAR NAMEPLATE', 'publicfiles/s03.png', '2019-11-19'),
(4, 'P001', 's04', 'DISTRIBUTION BOX', 'publicfiles/s04.png', '2019-11-19'),
(5, 'P001', 's05', 'BATTERY CHARGER', 'publicfiles/s05.png', '2019-11-19'),
(6, 'P001', 's06', 'BATTERY CHARGER NAMEPLATE', 'publicfiles/s06.png', '2019-11-19'),
(7, 'P001', 's07', 'CABLE TRAY ROUTE (PIW)', 'publicfiles/s07.png', '2019-11-19'),
(8, 'P001', 's08', 'CIVIL WORK LOCATION', 'publicfiles/s08.png', '2019-11-19'),
(9, 'P001', 's09', 'TX', 'publicfiles/s09.png', '2019-11-19'),
(10, 'P001', 's10', 'TX NAMEPLATE', 'publicfiles/s10.png', '2019-11-19'),
(11, 'P001', 's11', 'FEEDER & TX CABLE', 'publicfiles/s11.png', '2019-11-19'),
(12, 'P001', 's12', 'TRENCH', 'publicfiles/s12.png', '2019-11-19'),
(13, 'P001', 's13', 'LVDB', 'publicfiles/s13.png', '2019-11-19'),
(14, 'P001', 's14', 'GENSET LOCATION', 'publicfiles/s14.png', '2019-11-19'),
(15, 'P002', 's01', 'SUBSTATION', 'publicfiles/s01.png', '2019-11-19'),
(16, 'P002', 's02', 'EXISTING GEAR', 'publicfiles/s02.png', '2019-11-19'),
(17, 'P002', 's03', 'SWITCHGEAR NAMEPLATE', 'publicfiles/s03.png', '2019-11-19'),
(18, 'P002', 's04', 'DISTRIBUTION BOX', 'publicfiles/s04.png', '2019-11-19'),
(19, 'P002', 's05', 'BATTERY CHARGER', 'publicfiles/s05.png', '2019-11-19'),
(20, 'P002', 's06', 'BATTERY CHARGER NAMEPLATE', 'publicfiles/s06.png', '2019-11-19'),
(21, 'P002', 's07', 'CABLE TRAY ROUTE (PIW)', 'publicfiles/s07.png', '2019-11-19'),
(22, 'P002', 's08', 'CIVIL WORK LOCATION', 'publicfiles/s08.png', '2019-11-19'),
(23, 'P002', 's14', 'GENSET LOCATION', 'publicfiles/s09.png', '2019-11-19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `work_items`
--
ALTER TABLE `work_items`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `work_items`
--
ALTER TABLE `work_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
